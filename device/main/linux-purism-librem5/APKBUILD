# Reference: <https://postmarketos.org/vendorkernel>
# Maintainer:  Clayton Craft <clayton@craftyguy.net>
# Co-Maintainer: Alistair Francis <alistair@alistair23.me>
pkgname=linux-purism-librem5
pkgver=6.1.1
pkgrel=0
_purismrel=1
# <kernel ver>.<purism kernel release>
_purismver=${pkgver}pureos$_purismrel
pkgdesc="Purism Librem 5 phone kernel fork"
arch="aarch64"
_carch="arm64"
_flavor="purism-librem5"
url="https://source.puri.sm/Librem5/linux-next"
license="GPL-2.0-only"
options="!strip !check !tracedeps
	pmb:cross-native
	pmb:kconfigcheck-community
	"
makedepends="
	bash
	bison
	devicepkg-dev
	findutils
	flex
	installkernel
	openssl-dev
	perl
	rsync
	xz
	"
install="$pkgname.post-upgrade"

# Source
_repository="linux"
# kconfig generated with: ARCH=arm64 make defconfig KBUILD_DEFCONFIG=librem5_defconfig
_config="config-$_flavor.$arch"


source="
	$pkgname-$_purismver.tar.gz::https://source.puri.sm/Librem5/linux/-/archive/pureos/$_purismver/linux-pureos-$_purismver.tar.gz
	$_config
"
builddir="$srcdir/$_repository-pureos-$_purismver"

prepare() {
	default_prepare
	REPLACE_GCCH=0 \
		. downstreamkernel_prepare

	# NOTE: only for generating a config based on Purism's defconfig, for rebasing...
	# make ARCH="$_carch" CC="${CC:-gcc}" \
	# 	defconfig KBUILD_DEFCONFIG=librem5_defconfig
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-postmarketOS" \
		LOCALVERSION=".$_purismrel"
}

package() {
	downstreamkernel_package "$builddir" "$pkgdir" "$_carch" "$_flavor"

	make modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir/boot/dtbs"

}

sha512sums="
496d668cb75a60c74a147157d5d7b633c9e0ca52f0da141fc5f694960c3c37047918824b48ac35b8311221b1cc8c4fbf865ae298167d5b44aa9f85f4bae6b2d9  linux-purism-librem5-6.1.1pureos1.tar.gz
18826512ca9b78d61bda5d892afa31404dccc4be4492ff9a29e69d8c68edebc2ed0c470d6fe8d577f3840a5db0f79221e55ee334dd9b841d2cfd0a2ebe59c22a  config-purism-librem5.aarch64
"
